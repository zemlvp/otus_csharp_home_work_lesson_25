﻿using System;

namespace Prototype2
{
    /// <summary>
    /// Чулки
    /// </summary>
    [Serializable]
    public class Stockings : Socks
    {
        /// <summary>
        /// Сексуальность
        /// </summary>
        public int Sexy { get; set; }

        public Stockings(string color, int size, int sexy) 
            : base(color, size)
        {
            Sexy = sexy;
        }

        public override string ToString()
        {
            return $"Чулки: цвет={Color}, размер={Size}, сексуальность={Sexy}";
        }      
    }
}
