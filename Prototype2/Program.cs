﻿using System;

namespace Prototype2
{
    class Program
    {
        static void Main()
        {
            /*
             Иерархия классов:
             Носки - Socks (Цвет, Размер), 
             Чулки - Stockings (Сексуальность) наследуются от Носок, 
             Колготки - Tights (Теплота) наследуются от Чулок
             */

            Socks socks = new Socks("синий", 42);
            Socks deepClonedSocks = (Socks)socks.DeepCopy();
            Socks clonedSocks1 = (Socks)socks.Clone();
            Display(socks, deepClonedSocks, clonedSocks1);

            Stockings stockings = new Stockings("красный", 37, 70);
            Stockings deepClonedStockings = (Stockings)stockings.DeepCopy();
            Stockings clonedStockings1 = (Stockings)stockings.Clone();
            Display(stockings, deepClonedStockings, clonedStockings1);

            Tights tights = new Tights("красный", 37, 70, 45);
            Tights deepClonedTights = (Tights)tights.DeepCopy();
            Tights clonedTights1 = (Tights)tights.Clone();
            Display(tights, deepClonedTights, clonedTights1);

            Console.ReadKey();
        }

        private static void Display(Socks socks, Socks deepClonedSocks, Socks clonedSocks)
        {
            Console.WriteLine($"Оригинал: {socks}");
            Console.WriteLine($"Глубокое копирование: {deepClonedSocks}");
            Console.WriteLine($"Стандартный ICloneable: {clonedSocks}");
            Console.WriteLine(new string('-', 80));
            Console.WriteLine();
        }
    }
}