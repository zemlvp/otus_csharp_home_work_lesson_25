﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Prototype2
{
    /// <summary>
    /// Носки
    /// </summary>
    [Serializable]
    public class Socks : IMyCloneable, ICloneable
    {
        /// <summary>
        /// Цвет
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Размер
        /// </summary>
        public int Size { get; set; }

        public Socks(string color, int size)
        {
            Color = color;
            Size = size;                
        }

        public object DeepCopy()
        {
            object obj = null;
            using (MemoryStream tempStream = new MemoryStream())
            {
                BinaryFormatter binFormatter = new BinaryFormatter(null, new StreamingContext(StreamingContextStates.Clone));

                binFormatter.Serialize(tempStream, this);
                tempStream.Seek(0, SeekOrigin.Begin);

                obj = binFormatter.Deserialize(tempStream);
            }
            return obj;
        }

        public override string ToString()
        {
            return $"Носки: цвет={Color}, размер={Size}";
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
