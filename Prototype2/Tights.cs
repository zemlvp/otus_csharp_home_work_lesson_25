﻿using System;

namespace Prototype2
{
    /// <summary>
    /// Колготки
    /// </summary>
    [Serializable]
    public class Tights : Stockings
    {
        /// <summary>
        /// Теплота
        /// </summary>
        public int Warmth { get; set; }

        public Tights(string color, int size, int sexy, int warmth)
            : base(color, size, sexy)
        {
            Warmth = warmth;
        }

        public override string ToString()
        {
            return $"Колготки: цвет={Color}, размер={Size}, сексуальность={Sexy}, теплота={Warmth}";
        }
    }
}