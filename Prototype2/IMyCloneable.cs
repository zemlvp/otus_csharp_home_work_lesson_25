﻿namespace Prototype2
{
    public interface IMyCloneable
    {
        public object DeepCopy();
    }
}
